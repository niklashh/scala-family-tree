sealed trait Tree

final case class Node(children: Vector[Tree]) extends Tree

object Leaf extends Tree

/////////// MAIN

object Main extends App {

  // why does this have to be in main :C
  def atGen(gen: Int, tree: Tree): Int = {
    if (gen == 0) {
      1
    } else {
      tree match {
        case node: Node => node.children.map(atGen(gen - 1, _)).sum
        // coming across a leaf with gen not yet zero means that the
        // leaf has no children with the generation we are looking for
        case Leaf => 0
      }
    }
  }

  //
  // o
  //
  var tree: Tree = Leaf

  assert(atGen(0, tree) == 1)
  assert(atGen(1, tree) == 0)
  assert(atGen(2, tree) == 0)

  //
  //   o
  //  / \
  // o   o
  //
  tree = Node(Vector(Leaf, Leaf))

  assert(atGen(0, tree) == 1)
  assert(atGen(1, tree) == 2)
  assert(atGen(2, tree) == 0)

  //
  //            o-----------------
  //           / \                \
  //          o   o         -------o
  //         / \           /      / \
  //        o   o         o      o   o
  //           / \       / \    /   / \
  //    ------o   o     o   o  o   o   o
  //   / / / / \       / \    / \
  //  o o o o   o     o   o  o   o
  //           / \   / \    / \
  //          o   o o   o  o   o
  //         / \       / \    / \
  //        o   o     o   o  o   o
  //       /               \  \
  //      o                 o  o
  //                         \
  //                          o
  //
  tree = Node(Vector(Node(Vector(Leaf,Node(Vector(Node(Vector(Leaf,Leaf,Leaf,Leaf,Node(Vector(Node(Vector(Node(Vector(Leaf)),Leaf)),Leaf)))),Leaf)))),Leaf,Node(Vector(Node(Vector(Node(Vector(Node(Vector(Leaf,Node(Vector(Leaf,Node(Vector(Node(Vector(Leaf)))))))),Leaf)),Leaf)),Node(Vector(Node(Vector(Node(Vector(Leaf,Node(Vector(Node(Vector(Leaf)),Leaf)))),Leaf)))),Node(Vector(Leaf,Leaf))))))

  assert(atGen(0, tree) == 1)
  assert(atGen(1, tree) == 3)
  assert(atGen(2, tree) == 5)
  assert(atGen(3, tree) == 7)
  assert(atGen(4, tree) == 9)
  assert(atGen(5, tree) == 6)
  assert(atGen(6, tree) == 6)
  assert(atGen(7, tree) == 3)
  assert(atGen(8, tree) == 1)
  assert(atGen(9, tree) == 0)
}
